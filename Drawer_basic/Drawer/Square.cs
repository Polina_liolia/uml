﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace Drawer
{
    //ответственность: описывает понятие квадрат
    //центр и ширина
    //ответственность - раскрывется через агрегацию или композицию
    public class Square : IShape
    {
        protected MyDrawer drawer; //этот объект нарушает принцип единственносй ответственности класса //АГРЕГАЦИЯ
        protected int width;
        protected int x;
        protected int y;
        public string TypeShape { get; set; }

        public Square(MyDrawer dr, int x, int y, int width)
        {
            this.drawer = dr;
            this.x = x;
            this.y = y;
            this.width = width;
            TypeShape = "SQUARE";
        }
       
        
        public void Draw() //класс Rectangle зависит от Graphics через свойство в  MyDrawer, которое его содержит
        {
            drawer.Graphics.DrawRectangle(Pens.Red, (x - width/2), (y - width/2), width, width);
        }
    }
}
